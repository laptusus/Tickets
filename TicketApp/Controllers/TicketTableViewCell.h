//#import <UIKit/UIKit.h>
#import "../DataManager/DataManager.h"
#import "../DataManager/ApiManager.h"

@interface TicketTableViewCell : UITableViewCell

@property (nonatomic, strong) Ticket* ticket;
@property (nonatomic, strong) Ticket* favoriteTicket;

@end
