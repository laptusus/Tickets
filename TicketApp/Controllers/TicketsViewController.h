#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "TicketTableViewCell.h"

@interface TicketsViewController : UITableViewController
-(instancetype) initWithTickets: (NSArray*)tickets;
-(instancetype) initFavoriteTicketController;
@end
