//
//  FavoriteTicketMO+CoreDataProperties.m
//  
//
//  Created by Laptev Sasha on 17/05/2018.
//
//

#import "FavoriteTicketMO+CoreDataProperties.h"

@implementation FavoriteTicketMO (CoreDataProperties)

+ (NSFetchRequest<FavoriteTicketMO *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"FavoriteTicket"];
}

@dynamic airline;
@dynamic created;
@dynamic departure;
@dynamic expires;
@dynamic flightNumber;
@dynamic from;
@dynamic price;
@dynamic returnDate;
@dynamic to;

@end
